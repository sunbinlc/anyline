package org.anyline.entity;

import java.io.Serializable;

public interface AnyData extends Serializable {
    String toJSON();

}
